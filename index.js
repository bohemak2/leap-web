const palms = document.getElementById("palms");
const fingers = document.getElementById("fingers");
const cube = document.getElementById("cube");

/* INIT */
(function(){
  const cubeSize = 20;

  cube.style.top = (window.innerHeight - cubeSize) / 2;
  cube.style.left = (window.innerWidth - cubeSize) / 2 ;

  cube.style.height = cubeSize + "px";
  cube.style.width = cubeSize + "px";

  cube.style.borderRadius = cubeSize / 2 + "px";
})();

// https://www.root.cz/clanky/biologicke-algoritmy-5-neuronove-site/?ic=serial-box&icc=text-title

// LEAP V2/3 ORION (not 4): https://developer.leapmotion.com/sdk/v2
// https://www.blend4web.com/en/
// Right-handed coordinate system (https://di4564baj7skl.cloudfront.net/documentation/images/Leap_Axes.png) -> (0 = +/- horizontal, 1 = + depth, 2 = +/- vertical)
// https://developer-archive.leapmotion.com/documentation/javascript/devguide/Leap_Coordinate_Mapping.html
Leap.loop(function(frame) {

  // Clean elements
  palms.childNodes.forEach(p => p.remove());
  fingers.childNodes.forEach(f => f.remove());

  // Recreate elements
  frame.hands.forEach(h => {

    const palmPosition = frame.interactionBox.normalizePoint(h.palmPosition, true);
    const palmSize = 20 / palmPosition[1];

    const palm = pointAttribute(document.createElement("div"), palmPosition, palmSize);
    palm.className = "palm";

    palms.appendChild(palm);

    h.fingers.forEach(f => {
      const fingerPosition = frame.interactionBox.normalizePoint(f.tipPosition, true);
      let fingerSize = 10 / fingerPosition[1];

      const finger = pointAttribute(document.createElement("div"), fingerPosition, fingerSize);
      finger.className = "finger";

      fingers.appendChild(finger);

      /* Collision */
      const DETECT_AREA = 50,
            FINGER_AREA = 3;

      const fingerLeft = parseInt(finger.style.left) - FINGER_AREA / 2,
            fingerTop = parseInt(finger.style.top) - FINGER_AREA / 2,
            cubeLeft = parseInt(cube.style.left),
            cubeTop = parseInt(cube.style.top),
            cubeSize = parseInt(cube.style.width);
      fingerSize += FINGER_AREA / 2;

      if ( fingerLeft > cubeLeft - DETECT_AREA && fingerTop > cubeTop - DETECT_AREA && fingerLeft + fingerSize < cubeLeft + cubeSize + DETECT_AREA && fingerTop + fingerSize < cubeTop + cubeSize + DETECT_AREA) {
        const JUMP = 30;

        // Move left
        if (fingerLeft > cubeLeft && cubeLeft - JUMP >= 0){
          cube.style.left = cubeLeft - JUMP + "px";
        }

        // Move right
        if (fingerLeft < cubeLeft && cubeLeft + JUMP <= window.innerWidth){
          cube.style.left = cubeLeft + JUMP + "px";
        }

        // Move up
        if (fingerTop > cubeTop && cubeTop - JUMP >= 0){
          cube.style.top  = cubeTop - JUMP + "px";
        }

        // Move down
        if (fingerTop < cubeTop && cubeTop + JUMP <= window.innerHeight){
            cube.style.top  = cubeTop + JUMP + "px";
        }


      }

      // Other joint
      // const finger2Position = frame.interactionBox.normalizePoint(f.pipPosition, true);
      // fingers.appendChild(pointAttribute(finger, finger2Position, fingerSize));
    });
  });
});

function pointAttribute(el, position, size) {
  el.style.left = window.innerWidth * position[0] - size / 2 + "px";
  el.style.top = window.innerHeight * position[2] - size / 2 + "px";
  el.style.height = size + "px";
  el.style.width = size + "px";

  return el;
}
